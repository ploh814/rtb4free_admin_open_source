
require "elasticsearch"

ELASTICSEARCH_HOST = '172.30.0.81:9200'
ELASTICSEARCH_BIDDERLOGS_INDEX = "bidderlogs-*"

@client = Elasticsearch::Client.new log: false, host: ELASTICSEARCH_HOST

# Print status of the Elasticsearch cluster
# puts @client.cluster.health

#
queryStr = "type:wins"
now = Time.now.to_i
startTime = (now - 3600) * 1000
endTime = now  * 1000
maxSize=100
searchObj = {
          query: {
            bool: {
              must: [
                {
                  query_string: {
                    analyze_wildcard: true,
                    query: queryStr
                  }
                },
                {
                  range: {
                    "@timestamp": {
                      gte: startTime,
                      lte: endTime,
                      format: "epoch_millis"
                    }
                  }
                }
              ],
              must_not: []
            }
          },
          size: 0,
          aggs: {
            "4": {
              terms: {
                field: "adId.keyword",
                size: maxSize,
                order: {
                  _count: "desc"
                  }
              },
              aggs: {
                "3": {
                  sum: {
                    field: "price"
                    }
                }
              }
            }
          }
        }

response = @client.search index: ELASTICSEARCH_BIDDERLOGS_INDEX, body: searchObj

puts response.to_json

__END__

{"took"=>38, "timed_out"=>false, "_shards"=>{"total"=>120, "successful"=>120, "failed"=>0}, "hits"=>{"total"=>5123, "max_score"=>0.0, "hits"=>[]}, "aggregations"=>{"4"=>{"doc_count_error_upper_bound"=>0, "sum_other_doc_count"=>0, "buckets"=>[{"key"=>"18", "doc_count"=>5050, "3"=>{"value"=>6059.852170000115}}, {"key"=>"45", "doc_count"=>72, "3"=>{"value"=>108.00000000000001}}, {"key"=>"16", "doc_count"=>1, "3"=>{"value"=>2.0}}]}}}